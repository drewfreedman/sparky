provider "aws" {
  # AWS_ACCESS_KEY_ID       - defined as environment vars in CI/CD secrets
  # AWS_SECRET_ACCESS_KEY   - defined as environment vars in CI/CD secrets
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket  = "1-dsf-test"      # Manually created s3 bucket
    key     = "sparky.tfstate"  # File to save state in s3 as
    region  = "us-east-2"
  }
}

resource "aws_security_group" "ssh" {
  name        = "Allow SSH"
  description = "Allow SSH Connections"
  vpc_id      = "vpc-92866cf9"

  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "docker_host" {
  ami                    = "ami-0dacb0c129b49f529" # Amazon Linux 2 AMI
  instance_type          = "t2.micro"    # Free tier machine
  key_name               = "mytestkeys"
  vpc_security_group_ids = [aws_security_group.ssh.id]
}

data "template_file" "inventory_data" {
  template = file("./ansible_inventory.tpl")
  vars = {
    inv_data = join("\n", aws_instance.docker_host.*.public_ip)
  }
}

resource "local_file" "inventory" {
  content  = data.template_file.inventory_data.rendered
  filename = "inventory.ini"
}