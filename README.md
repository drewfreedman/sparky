# Sparky

This is a WIP project to play with Discord Bots.

The only file you will need in addition to what is in the repository is `.env`.

The `.env` is excluded from SCM by `.gitignore` so that secrets and environment specifics are not checked in.

Example contents of a `.env` for this project:

```buildoutcfg
DISCORD_TOKEN = "secret.token.-ra-ndom-Ch@r@ct3r5"
DISCORD_GUILD = "Madness By Marrowder"
```

Follow these [instructions](https://realpython.com/how-to-make-a-discord-bot-python/) to get a valid `DISCORD_TOKEN`