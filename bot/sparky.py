""" Initial attempt at learning discord.py functionality """
import os
import asyncio
import discord
import boto3
from random import choice
from botocore import UNSIGNED
from botocore.client import Config
from dotenv import load_dotenv
from datetime import datetime

# Ingest variables defined in .env
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

# New in discord.py>=1.5.0
# Must declare intents (scope / permissions) of the bot
intents = discord.Intents.default()
intents.members = True
intents.presences = True

# Connect to Discord
client = discord.Client(intents=intents)


# Functions to be used in decorators

async def create_temp_channel(prefix, message):
    """
    Used to create a temporary channel based on a user message.
    :param prefix: A string to prepend to the channel name
    :param message: The message object of the triggering message
    :return: temp - the voice channel object of the new, temporary channel.
    """
    # Check for temporary category, create it if it does not exist
    guild = message.author.guild
    category = discord.utils.get(guild.categories, name='Temporary')
    if not category:
        category = await guild.create_category('Temporary')
        return category

    # Create a temporary channel
    name = prefix + "-" + str(datetime.now())
    temp = await guild.create_voice_channel(name, category=category, bitrate=128000)
    return temp


async def move_vc_members(activity_name, dest, message):
    """
    Used to move members from an original shared VC to their own activity unique VC.
    :param dest:
    :param activity_name:
    :param message:
    :return: nil
    """
    channel = message.author.voice.channel
    for member in channel.members:
        if member.activity and member.activity.name == activity_name:
            await member.move_to(dest)

# Use decorators to extend discord.Client methods
@client.event
async def on_ready():
    """Plays mp3 in each active voice channel on bot start up"""
    guild = discord.utils.get(client.guilds, name="Madness By Marrowder")
    print(
        f' {client.user} has connected to  {guild.name} (id: {guild.id})'
    )

    for channel in guild.voice_channels:
        if channel.members:
            vcl = await channel.connect()
            uri = 'https://sparky-sounds.s3.amazonaws.com/Hello+peasants.mp3'
            vcl.play(discord.FFmpegPCMAudio(uri))
            while vcl.is_playing():
                await asyncio.sleep(1)
            await vcl.disconnect()

@client.event
async def on_typing(channel, user, when):
    """Prints when a user is typing in a guild channel"""
    print(
        f' {user} is typing now ({when})!'
    )

@client.event
async def on_member_join(member):
    """Sends a message to a DM to new members & prints message to system channel"""
    await member.create_dm()
    await member.dm_channel.send('Hey Pal! Welcome to the madness!')
    guild = discord.utils.get(client.guilds, name="Madness By Marrowder")
    channel = client.get_channel(guild.system_channel.id)
    await channel.send(f'{member.display_name} has joined the madness!')

@client.event
async def on_message(message):
    """Responds to text_channel prompts"""
    if message.author == client.user:
        return

    if message.content.startswith('Hello'):
        await message.channel.send('Haallooh?')

    if message.content.startswith('Heey Pal'):
        await message.channel.send('Can you do me a favor?')

    if message.content.startswith('joke'):
        # Connect to S3 anon
        s3 = boto3.client('s3', config=Config(signature_version=UNSIGNED))

        # List all objects in s3,
        response = s3.list_objects(Bucket='sparky-sounds')
        contents = response['Contents']

        # Filter keys by joke prefix, pick a random joke
        jokes = []
        for dict in contents:
            if 'cw-jokes' in (dict['Key']):
                jokes.append(dict['Key'])
        joke = choice(jokes)

        uri = "https://sparky-sounds.s3.amazonaws.com/" + joke

        vcl = await message.author.voice.channel.connect()
        vcl.play(discord.FFmpegPCMAudio(uri))
        while vcl.is_playing():
            await asyncio.sleep(1)
        await vcl.disconnect()

    if message.content.startswith('!EFT'):
        if message.author.voice:
            temp = await create_temp_channel("EFT", message)
            # Move EFT players in the same VC to the new temp channel
            await move_vc_members("Escape from Tarkov", temp, message)
        else:
            print(
                f' {message.author} is not in a channel'
            )

    if message.content.startswith('!temp'):
        if len(message.content.split()) > 1:
            name = message.content.split()[1]
        elif message.author.activity:
            name = message.author.activity.name
        else:
            name = message.author.display_name
        temp = await create_temp_channel(name, message)
        if message.author.voice and message.author.activity:
            await move_vc_members(message.author.activity.name, temp, message)

    if message.content.startswith('!cleanup'):
        guild = message.author.guild
        category = discord.utils.get(guild.categories, name='Temporary')
        if category and category.channels:
            for channel in category.channels:
                if not channel.members:
                    await channel.delete()

@client.event
async def on_voice_state_update(member, before, after):
    """Greets people who join voice channels"""
    guild = discord.utils.get(client.guilds, name="Madness By Marrowder")
    channel = client.get_channel(guild.system_channel.id)
    if after.channel and not member.bot:
        if not before.channel:
            vcl = await after.channel.connect()
            # Commenting out because he's sensitive.
            # if member.name == 'ZaBackinBlack':
            #     uri = 'https://sparky-sounds.s3.amazonaws.com/DJ_Sexfire.m4a'
            # else:
            uri = 'https://sparky-sounds.s3.amazonaws.com/Hello+peasants.mp3'
            vcl.play(discord.FFmpegPCMAudio(uri))
            while vcl.is_playing():
                await asyncio.sleep(1)
            await vcl.disconnect()
    if before.channel:
        if not before.channel.members and before.channel.category.name == 'Temporary':
            await before.channel.delete()

# Run bot with login token
client.run(TOKEN)
